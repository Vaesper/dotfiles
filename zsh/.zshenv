# .zshenv is always sourced, define here exported variables that should
# be available to other programs.

export VISUAL=vim
export EDITOR=vim
export PAGER=less
export PATH
export PYTHONPATH

pathappend() {
	for ARG in "$@"
	do
		if [ -d "$ARG" ] && [[ ":$PATH:" != *":$ARG:"* ]]; then
			PATH="${PATH:+"$PATH:"}$ARG"
		fi
	done
}

if (( $+commands[go] )); then
	export GOPATH
	GOPATH=$(go env GOPATH)
	pathappend ${HOME}/.local/bin ${GOPATH//://bin:}/bin
fi

if [[ $PYTHONPATH != ./.pip ]]; then
	PYTHONPATH=./.pip:$PYTHONPATH
fi

pathappend $HOME/.local/bin

# load zsh config files

env_config_files=(~/.zsh/**/*.zshenv(N))
if test ! -z "$env_config_files"; then
	for file in ${env_config_files}; do
		source $file
	done
fi

# export TERM="xterm-256color"

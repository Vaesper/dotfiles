# including this ensures that new gnome-terminal tabs keep the parent `pwd` !
if [ -e /etc/profile.d/vte.sh ]; then
    . /etc/profile.d/vte.sh
fi

#########
# zplug #
#########

# Check if zplug is installed
if [[ ! -d ${HOME}/.zplug ]]; then
	git clone https://github.com/zplug/zplug ${HOME}/.zplug
	source ${HOME}/.zplug/init.zsh && zplug update
else
	source ${HOME}/.zplug/init.zsh
fi

# Self-manage
zplug "zplug/zplug", hook-build:"zplug --self-manage"

# Sane defaults
zplug "willghatch/zsh-saneopt"

# Fish-like things
zplug "zsh-users/zsh-autosuggestions"
zplug "zsh-users/zsh-completions"
zplug "zdharma/fast-syntax-highlighting", defer:2
zplug "zsh-users/zsh-history-substring-search", defer:3

## All cool stuff from zimfw
zplug "zimfw/archive"
zplug "zimfw/archive", lazy:true
zplug "zimfw/completion"
zplug "zimfw/environment"
zplug "zimfw/git"
zplug "zimfw/git", lazy:true
zplug "zimfw/input"
zplug "zimfw/termtitle"
zplug "zimfw/utility"
zplug "zimfw/utility", lazy:true

zstyle ':zim:input' double-dot-expand yes
zstyle ':zim:termtitle' format '%(!.👽.👾)%n@%m: %~ $1'
zstyle ':zim:termtitle' hooks 'precmd' 'preexec'

# Colourization
zplug "zlsun/solarized-man"

# Oh-my-zsh plugins
zplug "plugins/colorize", from:oh-my-zsh
zplug "plugins/command-not-found", from:oh-my-zsh
zplug "plugins/extract", from:oh-my-zsh
zplug "plugins/vi-mode", from:oh-my-zsh

# Named directory helper
zplug "MikeDacre/cdbk"

# Theme
zplug "Vaesper/Vaesper-PL9K", from:gitlab
zplug "bhilburn/powerlevel9k", as:theme

zplug check || zplug install
zplug clean --force

# Then, source plugins and add commands to $PATH
zplug load

#############
# zplug end #
#############

# load zsh config files
config_files=(${HOME}/.zsh/**/*.zsh(N))
for file in ${config_files}; do
	source $file
done

TMOUT=5

TRAPALRM() {
	zle reset-prompt
}

bindkey "^[[3;5~" kill-word
bindkey "^H" backward-kill-word
bindkey "^[OA" history-substring-search-up
bindkey "^[OB" history-substring-search-down

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=14"
ZSH_AUTOSUGGEST_STRATEGY="match_prev_cmd"

eval `dircolors ~/.dir_colors`

alias pbcopy='xclip -selection clipboard'
alias pbpaste='xclip -selection clipboard -o'

alias xo='xdg-open'

echo -ne "\e]12;darkgray\a"

zstyle ":completion:*" matcher-list "m:{a-zA-Z}={A-Za-z}"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Zsh

    ├── .zsh
    ├── .dir_colors               Colourize ls
    ├── .zshenv                   Routine loading all .zshenv files
    └── .zshrc                    Routine loading all .zsh files

This package autoinstalls zplug and some awesome plugins.

### Customization

Others packages define environment variables or functions by writing shell files into `~/.zsh`.

`~/.zsshenv` sources all **.zshenv* files present in `~/.zsh` subfolders at zsh startup, and `~/.zshrc` do the same with **.zsh* files.

" vim-plug configuration
if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
		\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes

" Sensible defaults
Plug 'tpope/vim-sensible'

" Fully automatic indentation settings
Plug 'tpope/vim-sleuth'

" Comment lines
Plug 'tpope/vim-commentary'

" Subsitution
Plug 'tpope/vim-abolish'

" Status/tabline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Colourscheme
Plug 'altercation/vim-colors-solarized'

" Highlight/fix trailing whitespace
Plug 'ntpeters/vim-better-whitespace'

" Autotoggle relative numbers
Plug 'jeffkreeftmeijer/vim-numbertoggle'

" Rainbow parentheses
Plug 'luochen1990/rainbow'

Plug 'mhinz/vim-signify'
Plug 'mhinz/vim-startify'

" Syntax highlight stuff
Plug 'scrooloose/syntastic'
Plug 'stanangeloff/php.vim'
Plug 'brgmnn/vim-opencl'
Plug 'ekalinin/Dockerfile.vim'

Plug 'godlygeek/tabular'

Plug 'junegunn/vim-peekaboo'

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }

" Plug 'yuttie/comfortable-motion.vim'

Plug 'wincent/terminus'

Plug 'scrooloose/nerdtree'

" Plug 'pseewald/vim-anyfold'

Plug 'w0rp/ale'

" PHP
Plug 'rayburgemeestre/phpfolding.vim'
Plug 'ludovicchabant/vim-gutentags'
Plug 'stephpy/vim-php-cs-fixer'

" Golang
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

" LSP
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'prabirshrestha/asyncomplete-buffer.vim'
Plug 'prabirshrestha/asyncomplete-emoji.vim'
Plug 'prabirshrestha/asyncomplete-file.vim'

" Indent guides
Plug 'nathanaelkane/vim-indent-guides'
"Plug 'Yggdroot/indentLine'

" Initialize plugin system
call plug#end()

syntax enable

set background=light
colorscheme solarized

set hlsearch
set cursorline
set list
set number
set scrolloff=500 " Center cursor vertically
set linebreak

set showbreak=↪\
set listchars=tab:│⋯,eol:¬,nbsp:␣,trail:•,extends:»,precedes:«,space:·

aug customcommands
	au!
	au BufWinEnter,WinEnter * call matchadd('SpecialKey', '\s')
	au BufWinEnter,WinEnter * call matchadd('NonText', '$')

	au FileType javascript let g:ale_linters = {
		\ 'javascript': glob('.eslintrc*', '.;') != '' ? [ 'eslint' ] : [ 'standard' ],
		\ }

	au User lsp_setup call lsp#register_server({
		\ 'name': 'intelephense',
		\ 'cmd': {server_info->['node', expand('/usr/lib/node_modules/intelephense/lib/intelephense.js'), '--stdio']},
		\ 'initialization_options': {"storagePath": "/tmp/intelephense"},
		\ 'whitelist': ['php'],
		\ })

	au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#buffer#get_source_options({
		\ 'name': 'buffer',
		\ 'whitelist': ['*'],
		\ 'completor': function('asyncomplete#sources#buffer#completor'),
		\ }))

	au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#emoji#get_source_options({
		\ 'name': 'emoji',
		\ 'whitelist': ['*'],
		\ 'completor': function('asyncomplete#sources#emoji#completor'),
		\ }))
aug END

aug vagrant
	au!
	au BufRead,BufNewFile Vagrantfile set filetype=ruby
aug END

" Clear search
command C let @/=""

nnoremap <C-I> <C-a>
vnoremap <C-I> <C-a>

nnoremap tl :tabnext<CR>
nnoremap th :tabprev<CR>
nnoremap tn :tabnew<CR>
nnoremap to :tabnew ./<CR>

" lsp
nnoremap <leader>ltd :LspTypeDefinition<CR>
nnoremap <leader>ld  :LspDefinition<CR>
nnoremap <leader>lf  :LspDocumentFormat<CR>
nnoremap <leader>lh  :LspHover<CR>
nnoremap <leader>lr  :LspReferences<CR>
nnoremap <leader>lrn :LspRename<CR>
nnoremap <leader>ldd :LspDocumentDiagnostics<CR>

" asyncomplete
let g:asyncomplete_remove_duplicates = 1
let g:asyncomplete_smart_completion = 1
let g:asyncomplete_auto_popup = 1

inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<cr>"
imap <c-space> <Plug>(asyncomplete_force_refresh)

vmap <C-c> y: call system("xclip -i -selection clipboard", getreg("\""))<CR>
nnoremap <C-a> ggVG

nnoremap <C-j> :ALENextWrap<CR>
nnoremap <C-k> :ALEPreviousWrap<CR>

set splitbelow
set splitright

hi SpecialKey ctermfg=lightgray ctermbg=none
hi NonText    ctermfg=lightgray ctermbg=none

let g:airline_powerline_fonts = 1

let g:rainbow_active = 1

let g:rainbow_conf = {
\	'guifgs': ['royalblue3', 'darkorange3', 'seagreen3', 'firebrick'],
\	'ctermfgs': ['4', '3', '5', '6', '9', '13', '2', '1'],
\	'operators': '_,_',
\	'parentheses': ['start=/(/ end=/)/ fold', 'start=/\[/ end=/\]/ fold', 'start=/{/ end=/}/ fold'],
\	'separately': {
\		'*': {},
\		'tex': {
\			'parentheses': ['start=/(/ end=/)/', 'start=/\[/ end=/\]/'],
\		},
\		'vim': {
\			'parentheses': ['start=/(/ end=/)/', 'start=/\[/ end=/\]/', 'start=/{/ end=/}/ fold', 'start=/(/ end=/)/ containedin=vimFuncBody', 'start=/\[/ end=/\]/ containedin=vimFuncBody', 'start=/{/ end=/}/ fold containedin=vimFuncBody'],
\		},
\		'html': {
\			'parentheses': ['start=/\v\<((area|base|br|col|embed|hr|img|input|keygen|link|menuitem|meta|param|source|track|wbr)[ >])@!\z([-_:a-zA-Z0-9]+)(\s+[-_:a-zA-Z0-9]+(\=("[^"]*"|'."'".'[^'."'".']*'."'".'|[^ '."'".'"><=`]*))?)*\>/ end=#</\z1># fold'],
\		},
\		'css': 0,
\	}
\}

set tabstop=4
set shiftwidth=4
